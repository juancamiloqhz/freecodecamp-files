#! /bin/bash

if [[ $1 == "test" ]]
then
  PSQL="psql --username=postgres --dbname=worldcuptest -t --no-align -c"
else
  PSQL="psql --username=freecodecamp --dbname=worldcup -t --no-align -c"
fi

# Do not change code above this line. Use the PSQL variable above to query your database.

echo "$($PSQL "TRUNCATE games, teams")"

cat games.csv | while IFS="," read YEAR ROUND WINNER OPPONENT WINNER_GOALS OPPONENT_GOALS
do
  if [[ $YEAR != 'year' ]]
  then
    # ADD TEAMS BY WINNER
    WINNER_RESPONSE="$($PSQL "SELECT name FROM teams WHERE name='$WINNER'")"
    if [[ -z $WINNER_RESPONSE ]]
    then
      WINNER_RESPONSE="$($PSQL "INSERT INTO teams(name) VALUES('$WINNER')")"
      echo adding team WINNER, $WINNER
    fi
    # ADD TEAMS BY OPPONENT
    OPPONENT_RESPONSE="$($PSQL "SELECT name FROM teams WHERE name='$OPPONENT'")"
    if [[ -z $OPPONENT_RESPONSE ]]
    then
      OPPONENT_RESPONSE="$($PSQL "INSERT INTO teams(name) VALUES('$OPPONENT')")"
      echo adding team OPPONENT, $OPPONENT
    fi
    # ADD GAMES
    WINNER_ID="$($PSQL "SELECT team_id FROM teams WHERE name='$WINNER'")"
    OPPONENT_ID="$($PSQL "SELECT team_id FROM teams WHERE name='$OPPONENT'")"
    # echo WINNER ID: $WINNER_ID, OPPONENT ID: $OPPONENT_ID
    INSERT_GAME_RESPONSE="$($PSQL "INSERT INTO games(year,round,winner_id,opponent_id,winner_goals,opponent_goals) VALUES($YEAR,'$ROUND',$WINNER_ID,$OPPONENT_ID,$WINNER_GOALS,$OPPONENT_GOALS)")"
    echo inserting game, $YEAR - $ROUND - $WINNER - $OPPONENT - $WINNER_GOALS - $OPPONENT_GOALS
  fi
done 