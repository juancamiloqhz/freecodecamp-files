#!/bin/bash
PSQL="psql -X --username=freecodecamp --dbname=periodic_table --tuples-only -c"
NOT_FOUND_MESSAGE="I could not find that element in the database."

# Check if arg its a number
if [[ $1 =~ ^[0-9]+$ ]]
# If its a number query using atomic_number as a param
then
  ELEMENT_RESULT=$($PSQL "SELECT atomic_number,symbol,name,type,atomic_mass,melting_point_celsius,boiling_point_celsius FROM properties INNER JOIN elements USING(atomic_number) INNER JOIN types USING(type_id) WHERE atomic_number = $1")
  if [[ -z $ELEMENT_RESULT ]]
  then
    # If nothing found return NOT_FOUND_MESSAGE
    echo $NOT_FOUND_MESSAGE
  else
    echo $ELEMENT_RESULT | while read NUMBER BAR SYMBOL BAR NAME BAR TYPE BAR MASS BAR MELTING BAR BOILING
    do
      echo "The element with atomic number $NUMBER is $NAME ($SYMBOL). It's a $TYPE, with a mass of $MASS amu. $NAME has a melting point of $MELTING celsius and a boiling point of $BOILING celsius."
    done
  fi
# Check if arg its a word or letter
elif [[ $1 =~ ^[a-zA-Z]+$ ]]
then
  # If its a word or letter query using name or symbol as a param but with insensitive case
  ELEMENT_RESULT=$($PSQL "SELECT atomic_number,symbol,name,type,atomic_mass,melting_point_celsius,boiling_point_celsius FROM properties INNER JOIN elements USING(atomic_number) INNER JOIN types USING(type_id) WHERE symbol ILIKE '$1' OR name ILIKE '$1'")
  if [[ -z $ELEMENT_RESULT ]]
  # If nothing found return NOT_FOUND_MESSAGE
  then
    echo $NOT_FOUND_MESSAGE
  else
    echo $ELEMENT_RESULT | while read NUMBER BAR SYMBOL BAR NAME BAR TYPE BAR MASS BAR MELTING BAR BOILING
    do
      echo "The element with atomic number $NUMBER is $NAME ($SYMBOL). It's a $TYPE, with a mass of $MASS amu. $NAME has a melting point of $MELTING celsius and a boiling point of $BOILING celsius."
    done
  fi
else 
  echo "Please provide an element as an argument."
fi